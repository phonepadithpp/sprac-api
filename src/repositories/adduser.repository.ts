import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Adduser, AdduserRelations} from '../models';

export class AdduserRepository extends DefaultCrudRepository<
  Adduser,
  typeof Adduser.prototype.email,
  AdduserRelations
> {
  constructor(
    @inject('datasources.db') dataSource: MongodbDataSource,
  ) {
    super(Adduser, dataSource);
  }
}
