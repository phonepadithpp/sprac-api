import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Tracking, TrackingRelations} from '../models';

export class TrackingRepository extends DefaultCrudRepository<
  Tracking,
  typeof Tracking.prototype.trackid,
  TrackingRelations
> {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(Tracking, dataSource);
  }
}
