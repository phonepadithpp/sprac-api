import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Blestation, BlestationRelations} from '../models';

export class BlestationRepository extends DefaultCrudRepository<
  Blestation,
  typeof Blestation.prototype.stationid,
  BlestationRelations
> {
  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
  ) {
    super(Blestation, dataSource);
  }
}
