import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongodbDataSource} from '../datasources';
import {Edituser, EdituserRelations} from '../models';

export class EdituserRepository extends DefaultCrudRepository<
  Edituser,
  typeof Edituser.prototype.name,
  EdituserRelations
> {
  constructor(
    @inject('datasources.db') dataSource: MongodbDataSource,
  ) {
    super(Edituser, dataSource);
  }
}
