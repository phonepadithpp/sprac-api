import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Adduser} from './adduser.model';
import {Role} from './role.model';

@model()
export class UserRole extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @belongsTo(() => Adduser)
  userId: string;

  @belongsTo(() => Role)
  roleId: string;

  constructor(data?: Partial<UserRole>) {
    super(data);
  }
}

export interface UserRoleRelations {
  // describe navigational properties here
}

export type UserRolWithRelations = UserRole & UserRoleRelations;
