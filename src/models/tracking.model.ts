import { Entity, model, property } from '@loopback/repository';

@model()
export class Tracking extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  trackid: number;

  @property({
    type: 'string',
    required: true,
  })
  userid: string;

  @property({
    type: 'string',
    required: true,
  })
  timestampserver: string;

  @property({
    type: 'string',
    required: true,
  })
  timestampclient: string;

  @property({
    type: 'string',
    required: true,
  })
  stationid: string;

  @property({
    type: 'string',
    required: true,
  })
  signalpower: string;


  constructor(data?: Partial<Tracking>) {
    super(data);
  }
}

export interface TrackingRelations {
  // describe navigational properties here
}

export type TrackingWithRelations = Tracking & TrackingRelations;
