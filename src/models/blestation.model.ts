import {Entity, model, property} from '@loopback/repository';

@model()
export class Blestation extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  stationid: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  locationid: string;

  @property({
    type: 'string',
  })
  status?: string;


  constructor(data?: Partial<Blestation>) {
    super(data);
  }
}

export interface BlestationRelations {
  // describe navigational properties here
}

export type BlestationWithRelations = Blestation & BlestationRelations;
