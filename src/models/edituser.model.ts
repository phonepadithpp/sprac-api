import { Entity, model, property } from '@loopback/repository';

@model()
export class Edituser extends Entity {

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  surname: string;

  @property({
    type: 'string',
  })
  gender: string;

  @property({
    type: 'string',
  })
  address: string;

  @property({
    type: 'string',
  })
  idcard: string;

  @property({
    type: 'string',
  })
  idvacin: string;



  constructor(data?: Partial<Edituser>) {
    super(data);
  }
}

export interface EdituserRelations {
  // describe navigational properties here
}

export type EdituserWithRelations = Edituser & EdituserRelations;
