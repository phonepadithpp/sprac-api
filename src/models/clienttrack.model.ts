import { Entity, model, property } from '@loopback/repository';

@model()
export class Clienttrack extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  trackid: number;
  @property({
    type: 'string',
    required: true,
  })
  userid: string;

  @property({
    type: 'string',
    required: true,
  })
  timestampclient: string;

  @property({
    type: 'string',
    required: true,
  })
  stationid: string;

  @property({
    type: 'string',
    required: true,
  })
  signalpower: string;


  constructor(data?: Partial<Clienttrack>) {
    super(data);
  }
}

export interface ClienttrackRelations {
  // describe navigational properties here
}

export type ClienttrackWithRelations = Clienttrack & ClienttrackRelations;
