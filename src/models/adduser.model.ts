import { Entity, model, property } from '@loopback/repository';

@model()
export class Adduser extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    minLength: 8,
    pattern: '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{5,512})',
    maxLength: 512,
    required: true,
  })
  password: string;

  @property({
    type: 'string',
  })
  name: string;

  @property({
    type: 'string',
  })
  surname: string;

  @property({
    type: 'string',
  })
  gender: string;

  @property({
    type: 'string',
  })
  address: string;

  @property({
    type: 'string',
  })
  idcard: string;

  @property({
    type: 'string',
  })
  idvacin: string;

  @property({
    type: 'string',
  })
  role: string;



  constructor(data?: Partial<Adduser>) {
    super(data);
  }
}

export interface AdduserRelations {
  // describe navigational properties here
}

export type AdduserWithRelations = Adduser & AdduserRelations;
