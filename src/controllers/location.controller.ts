import { authenticate } from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import { Location } from '../models';
import { LocationRepository } from '../repositories';

export class LocationController {
  constructor(
    @repository(LocationRepository)
    public locationRepository: LocationRepository,
  ) { }

  @authenticate('jwt')
  @post('/locations')
  @response(200, {
    description: 'Location model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Location) } },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Location, {
            title: 'NewLocation',

          }),
        },
      },
    })
    location: Location,
  ): Promise<Location> {
    return this.locationRepository.create(location);
  }

  // @authenticate('jwt')
  // @get('/locations/count')
  // @response(200, {
  //   description: 'Location model count',
  //   content: { 'application/json': { schema: CountSchema } },
  // })
  // async count(
  //   @param.where(Location) where?: Where<Location>,
  // ): Promise<Count> {
  //   return this.locationRepository.count(where);
  // }

  @authenticate('jwt')
  @get('/locations')
  @response(200, {
    description: 'Array of Location model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Location, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.filter(Location) filter?: Filter<Location>,
  ): Promise<Location[]> {
    return this.locationRepository.find(filter);
  }



  // @authenticate('jwt')
  // @get('/locations/{id}')
  // @response(200, {
  //   description: 'Location model instance',
  //   content: {
  //     'application/json': {
  //       schema: getModelSchemaRef(Location, { includeRelations: true }),
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.string('id') id: string,
  //   @param.filter(Location, { exclude: 'where' }) filter?: FilterExcludingWhere<Location>
  // ): Promise<Location> {
  //   return this.locationRepository.findById(id, filter);
  // }


  @authenticate('jwt')
  @del('/locations/{id}')
  @response(204, {
    description: 'Location DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.locationRepository.deleteById(id);
  }
}
