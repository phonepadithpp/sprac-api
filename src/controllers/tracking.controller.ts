import { authenticate } from '@loopback/authentication';
var timediff = require('timediff');
const uniqueValues = require('unique-values')
import {
  Count,
  model,
  CountSchema,
  property,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import { Tracking, Clienttrack } from '../models';


import { TrackingRepository } from '../repositories';

const date = require('date-and-time');
@model()
export class CheckingRequest {
  @property({
    type: 'string',
    required: true,
  })
  stationid: string;
  @property({
    type: 'string',
    required: true,
  })
  timecheck: string;
}

@model()
export class Status {
  @property({
    type: 'string',
    required: false,
  })
  status: string;
}



export class TrackingController {
  constructor(
    @repository(TrackingRepository)
    public trackingRepository: TrackingRepository,
  ) { }


  //Function filter uniq user
  //function show profile
  async gettrackuser(stationid: string) {
    var alltrack = await this.trackingRepository.find({
      where: { stationid: stationid },
      order: ['trackid'],
    })
    return alltrack;
  }



  // @authenticate('jwt')
  // @post('/trackings')
  // @response(200, {
  //   description: 'Tracking model instance',
  //   content: { 'application/json': { schema: getModelSchemaRef(Tracking) } },
  // })
  // async create(
  //   @requestBody({
  //     content: {
  //       'application/json': {
  //         schema: getModelSchemaRef(Tracking, {
  //           title: 'NewTracking',

  //         }),
  //       },
  //     },
  //   })
  //   tracking: Tracking,
  // ): Promise<Tracking> {
  //   return this.trackingRepository.create(tracking);
  // }


  //@authenticate('jwt')
  @post('/trackings')
  @response(200, {
    description: 'Tracking model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Tracking) } },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Clienttrack, {
            title: 'NewTracking',
          }),
        },
      },
    })
    tracking: Tracking,
  ): Promise<Tracking> {
    const now = new Date();
    tracking.timestampserver = date.format(now, 'YYYY/MM/DD HH:mm:ss:SS');
    return this.trackingRepository.create(tracking);
  }

  @authenticate('jwt')
  @get('/trackings/count')
  @response(200, {
    description: 'Tracking model count',
    content: { 'application/json': { schema: CountSchema } },
  })
  async count(
    @param.where(Tracking) where?: Where<Tracking>,
  ): Promise<Count> {
    return this.trackingRepository.count(where);
  }

  //@authenticate('jwt')
  @get('/trackings')
  @response(200, {
    description: 'Array of Tracking model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Clienttrack),
        },
      },
    },
  })
  async find(
    @param.filter(Clienttrack) filter?: Filter<Clienttrack>,
  ): Promise<Clienttrack[]> {
    return this.trackingRepository.find();
  }


  // @authenticate('jwt')
  // @get('/trackings/{id}')
  // @response(200, {
  //   description: 'Tracking model instance',
  //   content: {
  //     'application/json': {
  //       schema: getModelSchemaRef(Tracking, { includeRelations: true }),
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.string('id') id: number,
  //   @param.filter(Tracking, { exclude: 'where' }) filter?: FilterExcludingWhere<Tracking>
  // ): Promise<Tracking> {
  //   return this.trackingRepository.findById(id, filter);
  // }


 // @authenticate('jwt')
 // @del('/trackings/{id}')
 // @response(204, {
 //   description: 'Tracking DELETE success',
 // })
 // async deleteById(@param.path.string('id') id: number): Promise<void> {
 //  await this.trackingRepository.deleteById(id);
 //}

  // Checking Place
  //@authenticate('jwt')
  @post('/trackings/checkplace')
  @response(200, {
    description: 'Checking Place by Id Station and Timestamp Format: YYYY/MM/DD HH:mm:ss, Safety level, Careful level, Danger level ',
    content: { 'application/json': { schema: getModelSchemaRef(Status) } },
  })
  async checkplace(
    @requestBody({
      CheckingRequest,
    })
    checkingrequest: CheckingRequest,
  ) {
    try {
    //Example: timediff('2021-05-09 20:15:10', '2021-05-09 20:42:10', 'm');
    var getalltrack = await this.gettrackuser(checkingrequest.stationid)
    var count = Object.keys(getalltrack).length;
    var gettime = [];
    var getusers = [];
    var convertime = [];
    var valueconvertime = [];
    for (var i = 0; i < count; i++) {
      gettime[i] = getalltrack[i].timestampclient
      getusers[i] = getalltrack[i].userid
      convertime[i] = timediff(gettime[i],  checkingrequest.timecheck, 'm');
      valueconvertime[i] = convertime[i].minutes
    }

    var filtervalueconvertime = valueconvertime.filter(minutes => minutes < 20);
    var limitusers = []
    for (var i = 1; i <= filtervalueconvertime.length; i++) {
      limitusers[i - 1] = getusers[getusers.length - i]
    }

    var filterusers = uniqueValues.getUniqueArrayValues(limitusers)
    const countfilterusers = Number(filterusers.length);

    var statusplace = ['Error!! Please Check BLE, Missing Request body format or Place']
    console.log(filterusers);

    if (countfilterusers < 2) {
       statusplace = ['Safety level']
    }
    if (countfilterusers >= 2 && countfilterusers <= 5) {
      statusplace = ['Careful level']
    }
    if (countfilterusers >= 5) {
      statusplace = ['Danger level']
    }
    //console.log(typeof countfilterusers)
    return statusplace;
    } catch (e) {
      console.error('ERROR!', e);
    }


  }

}
