// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import {authenticate, TokenService} from '@loopback/authentication';
import {validate} from 'secure-password-validator';
import {
  Credentials,
  MyUserService,
  TokenServiceBindings,
  User,
  UserRepository,
  UserServiceBindings,
} from '@loopback/authentication-jwt';
import {inject} from '@loopback/core';
import {model, property, repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef,
  post,
  requestBody,
  SchemaObject,
  patch,
} from '@loopback/rest';
import {SecurityBindings, securityId, UserProfile} from '@loopback/security';
import {genSalt, hash} from 'bcryptjs';
import _ from 'lodash';

//add new user
import {Adduser} from '../models/adduser.model';

//edit existing user
import {Edituser} from '../models/edituser.model';

@model()
export class NewUserRequest extends User {
  @property({
    type: 'string',
    required: true,
  })
  password: string;
}

@model()
export class UpdatePasswordUserRequest {
  @property({
    type: 'string',
    required: true,
  })
  newpassword: string;
  @property({
    type: 'string',
    required: true,
  })
  renewpassword: string;
}

// Credential for Login
const CredentialsSchema: SchemaObject = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
    },
    password: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const CredentialsRequestBody = {
  description: 'The input of login function',
  required: true,
  content: {
    'application/json': {schema: CredentialsSchema},
  },
};

// Credential for update password
const CredentialsUpdateSchema: SchemaObject = {
  type: 'object',
  required: [ 'newpassword', 'renewpassword'],
  properties: {
    newpassword: {
      type: 'string',
      minLength: 8,
    },
    renewpassword: {
      type: 'string',
      minLength: 8,
    },
  },
};

export const CredentialsUpdateRequestBody = {
  description: 'The input of update password',
  required: true,
  content: {
    'application/json': {schema: CredentialsUpdateSchema},
  },
};

export class UserController {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(SecurityBindings.USER, {optional: true})
    public user: UserProfile,
    @repository(UserRepository) protected userRepository: UserRepository,
  ) {}

  //function check user
  async checkUser(useremail: string) {
    var foundUser = await this.userRepository.findOne({
      where: {email: useremail},
    });

    if (foundUser) {
      console.log('found user)\n');
      return 1;
    } else {
      console.log('Not found user\n');
      return 0;
    }
  }

  //function show profile
  async showprofile(userid: string) {
    var profileuser = await this.userRepository.findOne({
      where: {id: userid},
    });
    return profileuser;
  }

  @post('/users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                profile: {
                  type: 'object',
                },
                token: {
                  type: 'string',
                },
              },
            },
          },
        },
      },
    },
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials) {
    //: Promise<{token: string}> {
    // ensure the user exists, and the password is correct
    const user = await this.userService.verifyCredentials(credentials);
    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.userService.convertToUserProfile(user);

    // Use function to show profile.
    const profile = await this.showprofile(userProfile.id);


    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    return {profile, token};
  }

  @authenticate('jwt')
  @get('/user/profile', {
    responses: {
      '200': {
        description: 'Return current user',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                profile: {
                  type: 'object',
                },
              },
            },
          },
        },
      },
    },
  })
  async profile(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ) {
    var profile = await this.showprofile(currentUserProfile[securityId]);
    return profile;
  }

  @post('/signup', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async signUp(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Adduser, {
            title: 'Register',
          }),
        },
      },
    })
    newUserRequest: NewUserRequest,
  ) {
    // ensure the user exists, and the password is correct
    //option password
    const options = {
      // options and its keys are optional
      // min password length, default = 8, cannot be less than 8
      minLength: 8,
      digits: true,
      letters: true,
      uppercase: true,
      symbols: true,
    };
    var result = validate(newUserRequest.password, options);
    if (result.valid == false) {
      var createError = require('http-errors');
      return createError(
        401,
        'Password is too weak or password is not sufficiently complex',
      );
    } else {
      //Check User
      var checkuser = await this.checkUser(newUserRequest.email);
      if (checkuser == 0) {
        try {
          var password = await hash(newUserRequest.password, await genSalt());
          var savedUser = await this.userRepository.create(
            _.omit(newUserRequest, 'password'),
          );
          newUserRequest.roles = 'user';
          await this.userRepository
            .userCredentials(savedUser.id)
            .create({password});
          console.log('SUCCESS! You have a new account :)\n');
          return savedUser;
        } catch (e) {
          console.error('ERROR!', e);
        }
      }
      if (checkuser == 1) {
        var createError = require('http-errors');
        return createError(
          401,
          'This user is already registered, please use a different email to register',
        );
      }
    }
  }

  @authenticate('jwt')
  @post('/user/changepassword', {
    responses: {
      '200': {
        description: 'Update Password',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async changepassword(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      UpdatePasswordUserRequest,
    })
    updatePasswordUserRequest: UpdatePasswordUserRequest,
  ) {
    // ensure the user exists, and the password is correct
    //option password
    const options = {
      // options and its keys are optional
      // min password length, default = 8, cannot be less than 8
      minLength: 8,
      digits: true,
      letters: true,
      uppercase: true,
      symbols: true,
    };

    var result = validate(updatePasswordUserRequest.newpassword, options);
    if (updatePasswordUserRequest.newpassword != updatePasswordUserRequest.renewpassword){
      var createError = require('http-errors');
      return createError(
        401,
        'The new password is not math to re-password',
      );
    }
    if (result.valid == false) {
      var createError = require('http-errors');
      return createError(
        401,
        'Password is too weak or password is not sufficiently complex',
      );
    } else {
      try {
        var password = await hash(
          updatePasswordUserRequest.newpassword,
          await genSalt(),
        );
        await this.userRepository
          .userCredentials(currentUserProfile[securityId])
          .patch({password});
        console.log('SUCCESS! Your password has been updated :)\n');
        return 'SUCCESS! Your password has been updated';
      } catch (e) {
        console.error('ERROR!', e);
      }
    }
  }

  @authenticate('jwt')
  @post('/user/updateprofile', {
    responses: {
      '200': {
        description: 'Update Profile',
        content: {
          'application/json': {
            schema: {
              type: 'string',
            },
          },
        },
      },
    },
  })
  async updateprofile(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
    @requestBody({
      Edituser,
    })
    newEdituser: Edituser,
  ) {
    try {
      var editUser = this.userRepository.updateById(
        currentUserProfile[securityId],
        newEdituser,
      );
      console.log('SUCCESS! Your profile has been updated :)\n');
      return 'SUCCESS! Your profile has been updated'
    } catch (e) {
      console.error('ERROR!', e);
    }
  }

}
