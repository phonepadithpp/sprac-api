import { authenticate } from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import { Blestation } from '../models';
import { BlestationRepository } from '../repositories';

export class BlestationController {
  constructor(
    @repository(BlestationRepository)
    public blestationRepository: BlestationRepository,
  ) { }

  @authenticate('jwt')
  @post('/blestations')
  @response(200, {
    description: 'Blestation model instance',
    content: { 'application/json': { schema: getModelSchemaRef(Blestation) } },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Blestation, {
            title: 'NewBlestation',

          }),
        },
      },
    })
    blestation: Blestation,
  ): Promise<Blestation> {
    return this.blestationRepository.create(blestation);
  }

  // @authenticate('jwt')
  // @get('/blestations/count')
  // @response(200, {
  //   description: 'Blestation model count',
  //   content: { 'application/json': { schema: CountSchema } },
  // })
  // async count(
  //   @param.where(Blestation) where?: Where<Blestation>,
  // ): Promise<Count> {
  //   return this.blestationRepository.count(where);
  // }

  @authenticate('jwt')
  @get('/blestations')
  @response(200, {
    description: 'Array of Blestation model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Blestation, { includeRelations: true }),
        },
      },
    },
  })
  async find(
    @param.filter(Blestation) filter?: Filter<Blestation>,
  ): Promise<Blestation[]> {
    return this.blestationRepository.find(filter);
  }


  // @authenticate('jwt')
  // @get('/blestations/{id}')
  // @response(200, {
  //   description: 'Blestation model instance',
  //   content: {
  //     'application/json': {
  //       schema: getModelSchemaRef(Blestation, { includeRelations: true }),
  //     },
  //   },
  // })
  // async findById(
  //   @param.path.string('id') id: string,
  //   @param.filter(Blestation, { exclude: 'where' }) filter?: FilterExcludingWhere<Blestation>
  // ): Promise<Blestation> {
  //   return this.blestationRepository.findById(id, filter);
  // }


  @authenticate('jwt')
  @del('/blestations/{id}')
  @response(204, {
    description: 'Blestation DELETE success',
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.blestationRepository.deleteById(id);
  }
}
