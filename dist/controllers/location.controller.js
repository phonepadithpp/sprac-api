"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let LocationController = class LocationController {
    constructor(locationRepository) {
        this.locationRepository = locationRepository;
    }
    async create(location) {
        return this.locationRepository.create(location);
    }
    // @authenticate('jwt')
    // @get('/locations/count')
    // @response(200, {
    //   description: 'Location model count',
    //   content: { 'application/json': { schema: CountSchema } },
    // })
    // async count(
    //   @param.where(Location) where?: Where<Location>,
    // ): Promise<Count> {
    //   return this.locationRepository.count(where);
    // }
    async find(filter) {
        return this.locationRepository.find(filter);
    }
    // @authenticate('jwt')
    // @get('/locations/{id}')
    // @response(200, {
    //   description: 'Location model instance',
    //   content: {
    //     'application/json': {
    //       schema: getModelSchemaRef(Location, { includeRelations: true }),
    //     },
    //   },
    // })
    // async findById(
    //   @param.path.string('id') id: string,
    //   @param.filter(Location, { exclude: 'where' }) filter?: FilterExcludingWhere<Location>
    // ): Promise<Location> {
    //   return this.locationRepository.findById(id, filter);
    // }
    async deleteById(id) {
        await this.locationRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.post('/locations'),
    rest_1.response(200, {
        description: 'Location model instance',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(models_1.Location) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Location, {
                    title: 'NewLocation',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Location]),
    tslib_1.__metadata("design:returntype", Promise)
], LocationController.prototype, "create", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.get('/locations'),
    rest_1.response(200, {
        description: 'Array of Location model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Location, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Location)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], LocationController.prototype, "find", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.del('/locations/{id}'),
    rest_1.response(204, {
        description: 'Location DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], LocationController.prototype, "deleteById", null);
LocationController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.LocationRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.LocationRepository])
], LocationController);
exports.LocationController = LocationController;
//# sourceMappingURL=location.controller.js.map