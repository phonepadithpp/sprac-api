import { Count, Filter, Where } from '@loopback/repository';
import { Tracking, Clienttrack } from '../models';
import { TrackingRepository } from '../repositories';
export declare class CheckingRequest {
    stationid: string;
    timecheck: string;
}
export declare class Status {
    status: string;
}
export declare class TrackingController {
    trackingRepository: TrackingRepository;
    constructor(trackingRepository: TrackingRepository);
    gettrackuser(stationid: string): Promise<(Tracking & import("../models").TrackingRelations)[]>;
    create(tracking: Tracking): Promise<Tracking>;
    count(where?: Where<Tracking>): Promise<Count>;
    find(filter?: Filter<Clienttrack>): Promise<Clienttrack[]>;
    checkplace(checkingrequest: CheckingRequest): Promise<string[] | undefined>;
}
