import { Filter } from '@loopback/repository';
import { Blestation } from '../models';
import { BlestationRepository } from '../repositories';
export declare class BlestationController {
    blestationRepository: BlestationRepository;
    constructor(blestationRepository: BlestationRepository);
    create(blestation: Blestation): Promise<Blestation>;
    find(filter?: Filter<Blestation>): Promise<Blestation[]>;
    deleteById(id: string): Promise<void>;
}
