import { TokenService } from '@loopback/authentication';
import { Credentials, MyUserService, User, UserRepository } from '@loopback/authentication-jwt';
import { SchemaObject } from '@loopback/rest';
import { UserProfile } from '@loopback/security';
import { Edituser } from '../models/edituser.model';
export declare class NewUserRequest extends User {
    password: string;
}
export declare class UpdatePasswordUserRequest {
    newpassword: string;
    renewpassword: string;
}
export declare const CredentialsRequestBody: {
    description: string;
    required: boolean;
    content: {
        'application/json': {
            schema: SchemaObject;
        };
    };
};
export declare const CredentialsUpdateRequestBody: {
    description: string;
    required: boolean;
    content: {
        'application/json': {
            schema: SchemaObject;
        };
    };
};
export declare class UserController {
    jwtService: TokenService;
    userService: MyUserService;
    user: UserProfile;
    protected userRepository: UserRepository;
    constructor(jwtService: TokenService, userService: MyUserService, user: UserProfile, userRepository: UserRepository);
    checkUser(useremail: string): Promise<0 | 1>;
    showprofile(userid: string): Promise<(User & import("@loopback/authentication-jwt").UserRelations) | null>;
    login(credentials: Credentials): Promise<{
        profile: (User & import("@loopback/authentication-jwt").UserRelations) | null;
        token: string;
    }>;
    profile(currentUserProfile: UserProfile): Promise<(User & import("@loopback/authentication-jwt").UserRelations) | null>;
    signUp(newUserRequest: NewUserRequest): Promise<any>;
    changepassword(currentUserProfile: UserProfile, updatePasswordUserRequest: UpdatePasswordUserRequest): Promise<any>;
    updateprofile(currentUserProfile: UserProfile, newEdituser: Edituser): Promise<"SUCCESS! Your profile has been updated" | undefined>;
}
