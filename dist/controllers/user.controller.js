"use strict";
// Copyright IBM Corp. 2020. All Rights Reserved.
// Node module: @loopback/example-todo-jwt
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = exports.CredentialsUpdateRequestBody = exports.CredentialsRequestBody = exports.UpdatePasswordUserRequest = exports.NewUserRequest = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const secure_password_validator_1 = require("secure-password-validator");
const authentication_jwt_1 = require("@loopback/authentication-jwt");
const core_1 = require("@loopback/core");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const security_1 = require("@loopback/security");
const bcryptjs_1 = require("bcryptjs");
const lodash_1 = tslib_1.__importDefault(require("lodash"));
//add new user
const adduser_model_1 = require("../models/adduser.model");
//edit existing user
const edituser_model_1 = require("../models/edituser.model");
let NewUserRequest = class NewUserRequest extends authentication_jwt_1.User {
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], NewUserRequest.prototype, "password", void 0);
NewUserRequest = tslib_1.__decorate([
    repository_1.model()
], NewUserRequest);
exports.NewUserRequest = NewUserRequest;
let UpdatePasswordUserRequest = class UpdatePasswordUserRequest {
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], UpdatePasswordUserRequest.prototype, "newpassword", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], UpdatePasswordUserRequest.prototype, "renewpassword", void 0);
UpdatePasswordUserRequest = tslib_1.__decorate([
    repository_1.model()
], UpdatePasswordUserRequest);
exports.UpdatePasswordUserRequest = UpdatePasswordUserRequest;
// Credential for Login
const CredentialsSchema = {
    type: 'object',
    required: ['email', 'password'],
    properties: {
        email: {
            type: 'string',
            format: 'email',
        },
        password: {
            type: 'string',
            minLength: 8,
        },
    },
};
exports.CredentialsRequestBody = {
    description: 'The input of login function',
    required: true,
    content: {
        'application/json': { schema: CredentialsSchema },
    },
};
// Credential for update password
const CredentialsUpdateSchema = {
    type: 'object',
    required: ['newpassword', 'renewpassword'],
    properties: {
        newpassword: {
            type: 'string',
            minLength: 8,
        },
        renewpassword: {
            type: 'string',
            minLength: 8,
        },
    },
};
exports.CredentialsUpdateRequestBody = {
    description: 'The input of update password',
    required: true,
    content: {
        'application/json': { schema: CredentialsUpdateSchema },
    },
};
let UserController = class UserController {
    constructor(jwtService, userService, user, userRepository) {
        this.jwtService = jwtService;
        this.userService = userService;
        this.user = user;
        this.userRepository = userRepository;
    }
    //function check user
    async checkUser(useremail) {
        var foundUser = await this.userRepository.findOne({
            where: { email: useremail },
        });
        if (foundUser) {
            console.log('found user)\n');
            return 1;
        }
        else {
            console.log('Not found user\n');
            return 0;
        }
    }
    //function show profile
    async showprofile(userid) {
        var profileuser = await this.userRepository.findOne({
            where: { id: userid },
        });
        return profileuser;
    }
    async login(credentials) {
        //: Promise<{token: string}> {
        // ensure the user exists, and the password is correct
        const user = await this.userService.verifyCredentials(credentials);
        // convert a User object into a UserProfile object (reduced set of properties)
        const userProfile = this.userService.convertToUserProfile(user);
        // Use function to show profile.
        const profile = await this.showprofile(userProfile.id);
        // create a JSON Web Token based on the user profile
        const token = await this.jwtService.generateToken(userProfile);
        return { profile, token };
    }
    async profile(currentUserProfile) {
        var profile = await this.showprofile(currentUserProfile[security_1.securityId]);
        return profile;
    }
    async signUp(newUserRequest) {
        // ensure the user exists, and the password is correct
        //option password
        const options = {
            // options and its keys are optional
            // min password length, default = 8, cannot be less than 8
            minLength: 8,
            digits: true,
            letters: true,
            uppercase: true,
            symbols: true,
        };
        var result = secure_password_validator_1.validate(newUserRequest.password, options);
        if (result.valid == false) {
            var createError = require('http-errors');
            return createError(401, 'Password is too weak or password is not sufficiently complex');
        }
        else {
            //Check User
            var checkuser = await this.checkUser(newUserRequest.email);
            if (checkuser == 0) {
                try {
                    var password = await bcryptjs_1.hash(newUserRequest.password, await bcryptjs_1.genSalt());
                    var savedUser = await this.userRepository.create(lodash_1.default.omit(newUserRequest, 'password'));
                    newUserRequest.roles = 'user';
                    await this.userRepository
                        .userCredentials(savedUser.id)
                        .create({ password });
                    console.log('SUCCESS! You have a new account :)\n');
                    return savedUser;
                }
                catch (e) {
                    console.error('ERROR!', e);
                }
            }
            if (checkuser == 1) {
                var createError = require('http-errors');
                return createError(401, 'This user is already registered, please use a different email to register');
            }
        }
    }
    async changepassword(currentUserProfile, updatePasswordUserRequest) {
        // ensure the user exists, and the password is correct
        //option password
        const options = {
            // options and its keys are optional
            // min password length, default = 8, cannot be less than 8
            minLength: 8,
            digits: true,
            letters: true,
            uppercase: true,
            symbols: true,
        };
        var result = secure_password_validator_1.validate(updatePasswordUserRequest.newpassword, options);
        if (updatePasswordUserRequest.newpassword != updatePasswordUserRequest.renewpassword) {
            var createError = require('http-errors');
            return createError(401, 'The new password is not math to re-password');
        }
        if (result.valid == false) {
            var createError = require('http-errors');
            return createError(401, 'Password is too weak or password is not sufficiently complex');
        }
        else {
            try {
                var password = await bcryptjs_1.hash(updatePasswordUserRequest.newpassword, await bcryptjs_1.genSalt());
                await this.userRepository
                    .userCredentials(currentUserProfile[security_1.securityId])
                    .patch({ password });
                console.log('SUCCESS! Your password has been updated :)\n');
                return 'SUCCESS! Your password has been updated';
            }
            catch (e) {
                console.error('ERROR!', e);
            }
        }
    }
    async updateprofile(currentUserProfile, newEdituser) {
        try {
            var editUser = this.userRepository.updateById(currentUserProfile[security_1.securityId], newEdituser);
            console.log('SUCCESS! Your profile has been updated :)\n');
            return 'SUCCESS! Your profile has been updated';
        }
        catch (e) {
            console.error('ERROR!', e);
        }
    }
};
tslib_1.__decorate([
    rest_1.post('/users/login', {
        responses: {
            '200': {
                description: 'Token',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                profile: {
                                    type: 'object',
                                },
                                token: {
                                    type: 'string',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody(exports.CredentialsRequestBody)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "login", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.get('/user/profile', {
        responses: {
            '200': {
                description: 'Return current user',
                content: {
                    'application/json': {
                        schema: {
                            type: 'object',
                            properties: {
                                profile: {
                                    type: 'object',
                                },
                            },
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "profile", null);
tslib_1.__decorate([
    rest_1.post('/signup', {
        responses: {
            '200': {
                description: 'User',
                content: {
                    'application/json': {
                        schema: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(adduser_model_1.Adduser, {
                    title: 'Register',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [NewUserRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "signUp", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.post('/user/changepassword', {
        responses: {
            '200': {
                description: 'Update Password',
                content: {
                    'application/json': {
                        schema: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.requestBody({
        UpdatePasswordUserRequest,
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, UpdatePasswordUserRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "changepassword", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.post('/user/updateprofile', {
        responses: {
            '200': {
                description: 'Update Profile',
                content: {
                    'application/json': {
                        schema: {
                            type: 'string',
                        },
                    },
                },
            },
        },
    }),
    tslib_1.__param(0, core_1.inject(security_1.SecurityBindings.USER)),
    tslib_1.__param(1, rest_1.requestBody({
        Edituser: edituser_model_1.Edituser,
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object, edituser_model_1.Edituser]),
    tslib_1.__metadata("design:returntype", Promise)
], UserController.prototype, "updateprofile", null);
UserController = tslib_1.__decorate([
    tslib_1.__param(0, core_1.inject(authentication_jwt_1.TokenServiceBindings.TOKEN_SERVICE)),
    tslib_1.__param(1, core_1.inject(authentication_jwt_1.UserServiceBindings.USER_SERVICE)),
    tslib_1.__param(2, core_1.inject(security_1.SecurityBindings.USER, { optional: true })),
    tslib_1.__param(3, repository_1.repository(authentication_jwt_1.UserRepository)),
    tslib_1.__metadata("design:paramtypes", [Object, authentication_jwt_1.MyUserService, Object, authentication_jwt_1.UserRepository])
], UserController);
exports.UserController = UserController;
//# sourceMappingURL=user.controller.js.map