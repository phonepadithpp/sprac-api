"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlestationController = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
let BlestationController = class BlestationController {
    constructor(blestationRepository) {
        this.blestationRepository = blestationRepository;
    }
    async create(blestation) {
        return this.blestationRepository.create(blestation);
    }
    // @authenticate('jwt')
    // @get('/blestations/count')
    // @response(200, {
    //   description: 'Blestation model count',
    //   content: { 'application/json': { schema: CountSchema } },
    // })
    // async count(
    //   @param.where(Blestation) where?: Where<Blestation>,
    // ): Promise<Count> {
    //   return this.blestationRepository.count(where);
    // }
    async find(filter) {
        return this.blestationRepository.find(filter);
    }
    // @authenticate('jwt')
    // @get('/blestations/{id}')
    // @response(200, {
    //   description: 'Blestation model instance',
    //   content: {
    //     'application/json': {
    //       schema: getModelSchemaRef(Blestation, { includeRelations: true }),
    //     },
    //   },
    // })
    // async findById(
    //   @param.path.string('id') id: string,
    //   @param.filter(Blestation, { exclude: 'where' }) filter?: FilterExcludingWhere<Blestation>
    // ): Promise<Blestation> {
    //   return this.blestationRepository.findById(id, filter);
    // }
    async deleteById(id) {
        await this.blestationRepository.deleteById(id);
    }
};
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.post('/blestations'),
    rest_1.response(200, {
        description: 'Blestation model instance',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(models_1.Blestation) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Blestation, {
                    title: 'NewBlestation',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Blestation]),
    tslib_1.__metadata("design:returntype", Promise)
], BlestationController.prototype, "create", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.get('/blestations'),
    rest_1.response(200, {
        description: 'Array of Blestation model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Blestation, { includeRelations: true }),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Blestation)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], BlestationController.prototype, "find", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.del('/blestations/{id}'),
    rest_1.response(204, {
        description: 'Blestation DELETE success',
    }),
    tslib_1.__param(0, rest_1.param.path.string('id')),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [String]),
    tslib_1.__metadata("design:returntype", Promise)
], BlestationController.prototype, "deleteById", null);
BlestationController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.BlestationRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.BlestationRepository])
], BlestationController);
exports.BlestationController = BlestationController;
//# sourceMappingURL=blestation.controller.js.map