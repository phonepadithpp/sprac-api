"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingController = exports.Status = exports.CheckingRequest = void 0;
const tslib_1 = require("tslib");
const authentication_1 = require("@loopback/authentication");
var timediff = require('timediff');
const uniqueValues = require('unique-values');
const repository_1 = require("@loopback/repository");
const rest_1 = require("@loopback/rest");
const models_1 = require("../models");
const repositories_1 = require("../repositories");
const date = require('date-and-time');
let CheckingRequest = class CheckingRequest {
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], CheckingRequest.prototype, "stationid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], CheckingRequest.prototype, "timecheck", void 0);
CheckingRequest = tslib_1.__decorate([
    repository_1.model()
], CheckingRequest);
exports.CheckingRequest = CheckingRequest;
let Status = class Status {
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: false,
    }),
    tslib_1.__metadata("design:type", String)
], Status.prototype, "status", void 0);
Status = tslib_1.__decorate([
    repository_1.model()
], Status);
exports.Status = Status;
let TrackingController = class TrackingController {
    constructor(trackingRepository) {
        this.trackingRepository = trackingRepository;
    }
    //Function filter uniq user
    //function show profile
    async gettrackuser(stationid) {
        var alltrack = await this.trackingRepository.find({
            where: { stationid: stationid },
            order: ['trackid'],
        });
        return alltrack;
    }
    // @authenticate('jwt')
    // @post('/trackings')
    // @response(200, {
    //   description: 'Tracking model instance',
    //   content: { 'application/json': { schema: getModelSchemaRef(Tracking) } },
    // })
    // async create(
    //   @requestBody({
    //     content: {
    //       'application/json': {
    //         schema: getModelSchemaRef(Tracking, {
    //           title: 'NewTracking',
    //         }),
    //       },
    //     },
    //   })
    //   tracking: Tracking,
    // ): Promise<Tracking> {
    //   return this.trackingRepository.create(tracking);
    // }
    //@authenticate('jwt')
    async create(tracking) {
        const now = new Date();
        tracking.timestampserver = date.format(now, 'YYYY/MM/DD HH:mm:ss:SS');
        return this.trackingRepository.create(tracking);
    }
    async count(where) {
        return this.trackingRepository.count(where);
    }
    //@authenticate('jwt')
    async find(filter) {
        return this.trackingRepository.find();
    }
    // @authenticate('jwt')
    // @get('/trackings/{id}')
    // @response(200, {
    //   description: 'Tracking model instance',
    //   content: {
    //     'application/json': {
    //       schema: getModelSchemaRef(Tracking, { includeRelations: true }),
    //     },
    //   },
    // })
    // async findById(
    //   @param.path.string('id') id: number,
    //   @param.filter(Tracking, { exclude: 'where' }) filter?: FilterExcludingWhere<Tracking>
    // ): Promise<Tracking> {
    //   return this.trackingRepository.findById(id, filter);
    // }
    // @authenticate('jwt')
    // @del('/trackings/{id}')
    // @response(204, {
    //   description: 'Tracking DELETE success',
    // })
    // async deleteById(@param.path.string('id') id: number): Promise<void> {
    //  await this.trackingRepository.deleteById(id);
    //}
    // Checking Place
    //@authenticate('jwt')
    async checkplace(checkingrequest) {
        try {
            //Example: timediff('2021-05-09 20:15:10', '2021-05-09 20:42:10', 'm');
            var getalltrack = await this.gettrackuser(checkingrequest.stationid);
            var count = Object.keys(getalltrack).length;
            var gettime = [];
            var getusers = [];
            var convertime = [];
            var valueconvertime = [];
            for (var i = 0; i < count; i++) {
                gettime[i] = getalltrack[i].timestampclient;
                getusers[i] = getalltrack[i].userid;
                convertime[i] = timediff(gettime[i], checkingrequest.timecheck, 'm');
                valueconvertime[i] = convertime[i].minutes;
            }
            var filtervalueconvertime = valueconvertime.filter(minutes => minutes < 20);
            var limitusers = [];
            for (var i = 1; i <= filtervalueconvertime.length; i++) {
                limitusers[i - 1] = getusers[getusers.length - i];
            }
            var filterusers = uniqueValues.getUniqueArrayValues(limitusers);
            const countfilterusers = Number(filterusers.length);
            var statusplace = ['Error!! Please Check BLE, Missing Request body format or Place'];
            console.log(filterusers);
            if (countfilterusers < 2) {
                statusplace = ['Safety level'];
            }
            if (countfilterusers >= 2 && countfilterusers <= 5) {
                statusplace = ['Careful level'];
            }
            if (countfilterusers >= 5) {
                statusplace = ['Danger level'];
            }
            //console.log(typeof countfilterusers)
            return statusplace;
        }
        catch (e) {
            console.error('ERROR!', e);
        }
    }
};
tslib_1.__decorate([
    rest_1.post('/trackings'),
    rest_1.response(200, {
        description: 'Tracking model instance',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(models_1.Tracking) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        content: {
            'application/json': {
                schema: rest_1.getModelSchemaRef(models_1.Clienttrack, {
                    title: 'NewTracking',
                }),
            },
        },
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [models_1.Tracking]),
    tslib_1.__metadata("design:returntype", Promise)
], TrackingController.prototype, "create", null);
tslib_1.__decorate([
    authentication_1.authenticate('jwt'),
    rest_1.get('/trackings/count'),
    rest_1.response(200, {
        description: 'Tracking model count',
        content: { 'application/json': { schema: repository_1.CountSchema } },
    }),
    tslib_1.__param(0, rest_1.param.where(models_1.Tracking)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], TrackingController.prototype, "count", null);
tslib_1.__decorate([
    rest_1.get('/trackings'),
    rest_1.response(200, {
        description: 'Array of Tracking model instances',
        content: {
            'application/json': {
                schema: {
                    type: 'array',
                    items: rest_1.getModelSchemaRef(models_1.Clienttrack),
                },
            },
        },
    }),
    tslib_1.__param(0, rest_1.param.filter(models_1.Clienttrack)),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [Object]),
    tslib_1.__metadata("design:returntype", Promise)
], TrackingController.prototype, "find", null);
tslib_1.__decorate([
    rest_1.post('/trackings/checkplace'),
    rest_1.response(200, {
        description: 'Checking Place by Id Station and Timestamp Format: YYYY/MM/DD HH:mm:ss, Safety level, Careful level, Danger level ',
        content: { 'application/json': { schema: rest_1.getModelSchemaRef(Status) } },
    }),
    tslib_1.__param(0, rest_1.requestBody({
        CheckingRequest,
    })),
    tslib_1.__metadata("design:type", Function),
    tslib_1.__metadata("design:paramtypes", [CheckingRequest]),
    tslib_1.__metadata("design:returntype", Promise)
], TrackingController.prototype, "checkplace", null);
TrackingController = tslib_1.__decorate([
    tslib_1.__param(0, repository_1.repository(repositories_1.TrackingRepository)),
    tslib_1.__metadata("design:paramtypes", [repositories_1.TrackingRepository])
], TrackingController);
exports.TrackingController = TrackingController;
//# sourceMappingURL=tracking.controller.js.map