import { Filter } from '@loopback/repository';
import { Location } from '../models';
import { LocationRepository } from '../repositories';
export declare class LocationController {
    locationRepository: LocationRepository;
    constructor(locationRepository: LocationRepository);
    create(location: Location): Promise<Location>;
    find(filter?: Filter<Location>): Promise<Location[]>;
    deleteById(id: string): Promise<void>;
}
