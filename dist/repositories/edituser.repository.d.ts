import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Edituser, EdituserRelations } from '../models';
export declare class EdituserRepository extends DefaultCrudRepository<Edituser, typeof Edituser.prototype.name, EdituserRelations> {
    constructor(dataSource: MongodbDataSource);
}
