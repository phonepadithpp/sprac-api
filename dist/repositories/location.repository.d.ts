import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Location, LocationRelations } from '../models';
export declare class LocationRepository extends DefaultCrudRepository<Location, typeof Location.prototype.locationid, LocationRelations> {
    constructor(dataSource: MongodbDataSource);
}
