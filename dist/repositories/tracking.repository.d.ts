import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Tracking, TrackingRelations } from '../models';
export declare class TrackingRepository extends DefaultCrudRepository<Tracking, typeof Tracking.prototype.trackid, TrackingRelations> {
    constructor(dataSource: MongodbDataSource);
}
