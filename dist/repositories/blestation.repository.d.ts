import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Blestation, BlestationRelations } from '../models';
export declare class BlestationRepository extends DefaultCrudRepository<Blestation, typeof Blestation.prototype.stationid, BlestationRelations> {
    constructor(dataSource: MongodbDataSource);
}
