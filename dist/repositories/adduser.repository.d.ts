import { DefaultCrudRepository } from '@loopback/repository';
import { MongodbDataSource } from '../datasources';
import { Adduser, AdduserRelations } from '../models';
export declare class AdduserRepository extends DefaultCrudRepository<Adduser, typeof Adduser.prototype.email, AdduserRelations> {
    constructor(dataSource: MongodbDataSource);
}
