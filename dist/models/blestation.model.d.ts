import { Entity } from '@loopback/repository';
export declare class Blestation extends Entity {
    stationid: string;
    name: string;
    locationid: string;
    status?: string;
    constructor(data?: Partial<Blestation>);
}
export interface BlestationRelations {
}
export declare type BlestationWithRelations = Blestation & BlestationRelations;
