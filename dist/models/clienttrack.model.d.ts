import { Entity } from '@loopback/repository';
export declare class Clienttrack extends Entity {
    trackid: number;
    userid: string;
    timestampclient: string;
    stationid: string;
    signalpower: string;
    constructor(data?: Partial<Clienttrack>);
}
export interface ClienttrackRelations {
}
export declare type ClienttrackWithRelations = Clienttrack & ClienttrackRelations;
