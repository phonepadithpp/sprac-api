import { Entity } from '@loopback/repository';
export declare class Tracking extends Entity {
    trackid: number;
    userid: string;
    timestampserver: string;
    timestampclient: string;
    stationid: string;
    signalpower: string;
    constructor(data?: Partial<Tracking>);
}
export interface TrackingRelations {
}
export declare type TrackingWithRelations = Tracking & TrackingRelations;
