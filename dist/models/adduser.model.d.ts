import { Entity } from '@loopback/repository';
export declare class Adduser extends Entity {
    email: string;
    password: string;
    name: string;
    surname: string;
    gender: string;
    address: string;
    idcard: string;
    idvacin: string;
    role: string;
    constructor(data?: Partial<Adduser>);
}
export interface AdduserRelations {
}
export declare type AdduserWithRelations = Adduser & AdduserRelations;
