"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Blestation = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let Blestation = class Blestation extends repository_1.Entity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        id: true,
        generated: false,
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Blestation.prototype, "stationid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Blestation.prototype, "name", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Blestation.prototype, "locationid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
    }),
    tslib_1.__metadata("design:type", String)
], Blestation.prototype, "status", void 0);
Blestation = tslib_1.__decorate([
    repository_1.model(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Blestation);
exports.Blestation = Blestation;
//# sourceMappingURL=blestation.model.js.map