import { Entity } from '@loopback/repository';
export declare class Edituser extends Entity {
    name: string;
    surname: string;
    gender: string;
    address: string;
    idcard: string;
    idvacin: string;
    constructor(data?: Partial<Edituser>);
}
export interface EdituserRelations {
}
export declare type EdituserWithRelations = Edituser & EdituserRelations;
