import { Entity } from '@loopback/repository';
export declare class Location extends Entity {
    locationid: string;
    place: string;
    constructor(data?: Partial<Location>);
}
export interface LocationRelations {
}
export declare type LocationWithRelations = Location & LocationRelations;
