"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Clienttrack = void 0;
const tslib_1 = require("tslib");
const repository_1 = require("@loopback/repository");
let Clienttrack = class Clienttrack extends repository_1.Entity {
    constructor(data) {
        super(data);
    }
};
tslib_1.__decorate([
    repository_1.property({
        type: 'number',
        id: true,
        generated: true,
    }),
    tslib_1.__metadata("design:type", Number)
], Clienttrack.prototype, "trackid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Clienttrack.prototype, "userid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Clienttrack.prototype, "timestampclient", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Clienttrack.prototype, "stationid", void 0);
tslib_1.__decorate([
    repository_1.property({
        type: 'string',
        required: true,
    }),
    tslib_1.__metadata("design:type", String)
], Clienttrack.prototype, "signalpower", void 0);
Clienttrack = tslib_1.__decorate([
    repository_1.model(),
    tslib_1.__metadata("design:paramtypes", [Object])
], Clienttrack);
exports.Clienttrack = Clienttrack;
//# sourceMappingURL=clienttrack.model.js.map